package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {

    //Disclaimer: Jeg kunne ikke få algoritmen til at pass 'faster' testen når den var i en metode,
    //så jeg har forsøgt med en hjælpemetode, hvilket hjalp.

    @Override
    public long sum(List<Long> list) {
        //returnerer hjælpemetoden
        return sumRecursive(list, 0, 0);
    }

    private long sumRecursive(List<Long> list, int index, long currentSum) {
        //Hvis indeksen er lig længden af listen, returner vi summen
        if (index == list.size()) {
            return currentSum;
        }
        //Tilføjer det nuværende element til summen og går videre til næste indeks i liseten
        long newSum = currentSum + list.get(index);
        return sumRecursive(list, index + 1, newSum);
    }
}
