package INF102.lab3.numberGuesser;


public class MyGeniusGuesser implements IGuesser {

    @Override
    public int findNumber(RandomNumber number) {
        int lowerBound = 0;
        int upperBound = number.getUpperbound();
        int guess;

        while (true) {
            //Gætter i midten af talrækken
            guess = (lowerBound + upperBound) / 2;
            int result = number.guess(guess);

            if (result == 0) {
                //Hvis gættet er korrekt, returner gættet
                return guess;
            } else if (result == -1) {
                //Hvis gættet er for lavt, juster nedre grænse
                lowerBound = guess + 1;
            } else {
                //Hvis gættet er for højt justerer vi øvre grænse
                upperBound = guess - 1;
            }
        }
    }
}


