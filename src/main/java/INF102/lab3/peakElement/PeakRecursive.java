package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
        //Tjekker først om listen er tom
        if (numbers.isEmpty())
            throw new IllegalArgumentException("List is empty");

        //Retuerner hjælpemetoden
        return findPeak(numbers, 0, numbers.size() - 1);
    }

    private int findPeak(List<Integer> numbers, int left, int right) {
        //  Hvis to indekser er ens, returner det ene indeks
        if (left == right)
            return numbers.get(left);
        //Udregner det miderste indeks af den nuværende del af arrayet
        int mid = left + (right - left) / 2;

        //Sammenligner elementet på midten med det næste element
        if (numbers.get(mid) > numbers.get(mid + 1))
            return findPeak(numbers, left, mid);
        else
            return findPeak(numbers, mid + 1, right);
    }
}
